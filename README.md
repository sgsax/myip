# MyIP

Simple page to display remote user's IP and hostname

Includes css so you can customize your font face and color if you like that kindof thing

## Caveats
- only good for local subnet, doesn't handle NAT or proxy
- must have reverse DNS enabled for your IPs
